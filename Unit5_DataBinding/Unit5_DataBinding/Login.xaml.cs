﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinUniversity.Interfaces;

namespace Unit5_DataBinding
{
    
    public partial class Login : ContentPage
    {
        public Login(INavigationService nav)
        {
            this.BindingContext = new LoginViewModel() { Navigation = nav };
            InitializeComponent();
          
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            LoginViewModel model = this.BindingContext as LoginViewModel;
            model.GoInfo();
        }
    }
}
