﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using XamarinUniversity.Interfaces;
using XamarinUniversity.Services;

namespace Unit5_DataBinding
{
    public partial class App : Application
    {

        public App()
        {
            FormsNavigationPageService service = new FormsNavigationPageService();
            service.RegisterPage(AppPage.Info, () => {
                return new Info(); });
            service.RegisterPage(AppPage.Login, () => new Login(service));


            InitializeComponent();
            this.MainPage = new NavigationPage(new Login(service));

        }


    }
}
